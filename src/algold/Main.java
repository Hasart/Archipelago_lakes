package algold;

import java.io.*;

/**
 * Created by juras on 23.03.2017.
 */
public class Main {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static int w, h;
    static char[][] map;
    static int[] pos = {0, 0};
    static int sumIslands = 0;

    private static void printMap(char[][] map) {
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                System.out.print(map[i][j]);
            }
            System.out.println();
        }
    }

    private static char[][] input() throws IOException {
        char temp;
        br = new BufferedReader(new InputStreamReader(new FileInputStream("C:\\Users\\juras\\IdeaProjects\\alg3\\src\\algold\\pub04.in")));
        String[] line = br.readLine().split(" ");
        w = Integer.parseInt(line[0]);
        h = Integer.parseInt(line[1]);
        char[][] map = new char[h][w];
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                do {
                    temp = (char) br.read();
                    if (temp == '.' || temp == 'X') {
                        break;
                    }
                } while (true);

                map[i][j] = temp;

            }
        }
        return map;
    }

    public static void main(String[] args) throws IOException {
        map = input();
     //   printMap(map);
        markWater('o', 0, 0);
        findIsland();

        System.out.println();
     //   printMap(map);
        System.out.println("Sum of islands is: " + sumIslands);

    }

    private static void findIsland() {
        for (int i = 0; i < w; i++) {
            pos[0] = 0;
            pos[1] = i;
            while (moveDown()) {
                if (getField() == '.') {

                } else if (getField() == 'X') {
                    roundIsland(pos[0], pos[1]);
                    if (getField(pos[0] - 1, pos[1]) == 'o') {
                        sumIslands++;
                    }
                }
            }
        }
    }

    private static void markWater(char replacement, int x, int y) {
        if (isValidCoords(x, y) && getField(x, y) == '.') {
            setField(replacement, x, y);

            markWater(replacement, x + 1, y);
            markWater(replacement, x - 1, y);
            markWater(replacement, x, y - 1);
            markWater(replacement, x, y + 1);

//            markWater(replacement, x + 1, y + 1);
//            markWater(replacement, x - 1, y + 1);
         //   markWater(replacement, x - 1, y - 1);
           // markWater(replacement, x + 1, y - 1);

        }


    }

    private static void roundIsland(int x, int y) {
        if (isValidCoords(x, y) && getField(x, y) == 'X') {
            setField('I', x, y);

            roundIsland(x + 1, y);
            roundIsland(x - 1, y);
            roundIsland(x, y - 1);
            roundIsland(x, y + 1);

            roundIsland(x + 1, y + 1);
            roundIsland(x - 1, y + 1);
            roundIsland(x - 1, y - 1);
            roundIsland(x + 1, y - 1);
        }


    }

    private static char getField() {
        return map[pos[0]][pos[1]];
    }

    private static char getField(int x, int y) {
        return map[x][y];
    }

    private static char[][] setField(char a) {
        map[pos[0]][pos[1]] = a;
        return map;
    }

    private static char[][] setField(char a, int x, int y) {
        map[x][y] = a;
        return map;
    }

    private static boolean moveLeft() {
        if (!isValidCoords(pos[0], pos[1]--)) {
            return false;
        }
        pos[1]--;
        return true;
    }

    private static boolean moveUp() {
        if (!isValidCoords(pos[0]--, pos[1])) {
            return false;
        }
        pos[0]--;
        return true;
    }

    private static boolean moveRight() {
        if (!isValidCoords(pos[0], pos[1]++)) {
            return false;
        }
        pos[1]++;
        return true;
    }

    private static boolean moveDown() {
        if (!isValidCoords(pos[0] + 1, pos[1])) {
            return false;
        }
        pos[0]++;
        return true;
    }

    private static void printPos() {
        System.out.println("[" + pos[0] + ", " + pos[1] + "]" + " field is:'" + getField() + "'");
    }

    static boolean isValidCoords() {
        int x = pos[0];
        int y = pos[1];
        return x >= 0 && y >= 0 && x < w && y < h;
    }

    static boolean isValidCoords(int x, int y) {
        return x >= 0 && y >= 0 && x < h && y < w;
    }
}
