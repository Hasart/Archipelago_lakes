package alg;


import java.io.*;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


/**
 * Created by juras on 23.03.2017.
 */

@SuppressWarnings("Duplicates")
public class Main {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static int w, h;
    static char[][] map;
    static int tempSum = 0;
    static int sumIslands = 0;

    static ArrayList<Integer> array = new ArrayList<Integer>();
    static ArrayDeque<Integer> lakes = new ArrayDeque<Integer>();
    static ArrayDeque<Integer> ocean = new ArrayDeque<Integer>();
    static ArrayDeque<Integer> elake = new ArrayDeque<Integer>();
    static ArrayDeque<Integer> stackI = new ArrayDeque<Integer>();
    static ArrayDeque<Integer> island = new ArrayDeque<Integer>();
    static ArrayDeque<Integer> lakeFoundlist = new ArrayDeque<Integer>();


    private static void printMap(char[][] map) {
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                System.out.print(map[i][j]);
            }
            System.out.println();
        }
    }

    private static char[][] input() throws IOException {
//        br = new BufferedReader(new InputStreamReader(new FileInputStream("C:\\Users\\juras\\" +
//                "IdeaProjects\\alg3\\src\\algold\\pub06.in")));
        String[] line = br.readLine().split(" ");
        w = Integer.parseInt(line[0]);
        h = Integer.parseInt(line[1]);
        char[][] map = new char[h][w];
//        for (int i = 0; i < h; i++) {
//            for (int j = 0; j < w; j++) {
//                do {
//                    temp = (char) br.read();
//                    if (temp == '.' || temp == 'X') {
//                        break;
//                    }
//                } while (true);
////                if (i == 0 || j == 0 || i == h - 1 || j == w - 1) {
//                //  map[i][j] = 'o';
////                } else {
//                map[i][j] = temp;
////                }
//            }
//        }

//        for (int i = 0; i < h; i++) {
//            String values = br.readLine();
//
//            for (int j = 0; j < w; j++) {
//                map[i][j] = values.charAt(0);values=values.substring(1);
//            }
//        }

        br.readLine();

        for (int i = 1; i < h - 1; i++) {
            String values = br.readLine();

            for (int j = 1; j < w - 1; j++) {
                map[i][j] = values.charAt(j);
            }

        }
        return map;
    }

    public static void main(String[] args) throws IOException {
        map = input();
        exploreOcean6();
//        System.exit(0);
//        printMap(map);
        findIsland();

        System.out.print(sumIslands + " ");
//        Collections.sort(array, Collections.reverseOrder());
        Collections.sort(array, new Comparator<Integer>() {
            @Override
            public int compare(Integer lhs, Integer rhs) {
                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                return lhs > rhs ? -1 : (lhs < rhs) ? 1 : 0;
            }
        });
        for (int e : array) {
            System.out.printf("%d ", e);
        }

    }

    private static void findIsland2() {
        int i, j;
        while (island.size() >= 2) {
            j = island.pop();
            i = island.pop();
            if (map[i][j] == 'X') {
                tempSum = 0;
                sumIslands++;
                roundIsland2(i, j);
                while (!lakes.isEmpty()) {
                    exploreLake3('L', '`', lakes.pop(), lakes.pop());
                }
                array.add(tempSum);

            }
        }
    }

    private static void findIsland() {
        for (int i = 1; i < h - 1; i++) {
            for (int j = 1; j < w - 1; j++) {
                if (map[i][j] == 'X') {
                    tempSum = 0;
                    sumIslands++;
                    roundIsland2(i, j);
//                    while (!lakes.isEmpty()) {
//                        exploreLake3('L', '`', lakes.pop(), lakes.pop());
//                    }
                    array.add(tempSum);
                }
            }
        }
    }

    private static void exploreLake3(char replace, char replacement, int i, int j) {
        if (map[i][j] != replace) {
            return;
        }
        map[i][j] = replacement;
        elake.push(i);
        elake.push(j);

        while (elake.size() >= 2) {
            j = elake.pop();
            i = elake.pop();

            if (map[i][j - 1] == replace) {
                map[i][j - 1] = replacement;
                elake.push(i);
                elake.push(j - 1);
            } else if (map[i][j - 1] == 'X') {
                stackI.push(i);
                stackI.push(j - 1);
                map[i][j - 1] = 'I';
                roundIsland(i,j-1);
            }
            if (map[i][j + 1] == replace) {
                map[i][j + 1] = replacement;
                elake.push(i);
                elake.push(j + 1);
            } else if (map[i][j + 1] == 'X') {
                stackI.push(i);
                map[i][j + 1] = 'I';
                stackI.push(j + 1);
                roundIsland(i,j+1);
            }
            if (map[i - 1][j] == replace) {
                map[i - 1][j] = replacement;
                elake.push(i - 1);
                elake.push(j);
            } else if (map[i - 1][j] == 'X') {
                stackI.push(i - 1);
                map[i - 1][j] = 'I';
                stackI.push(j);
                roundIsland(i-1,j);
            }
            if (map[i + 1][j] == replace) {
                map[i + 1][j] = replacement;
                elake.push(i + 1);
                elake.push(j);
            } else if (map[i + 1][j] == 'X') {
                stackI.push(i + 1);
                map[i + 1][j] = 'I';
                stackI.push(j);
                roundIsland(i+1,j);
            }
            if (map[i - 1][j - 1] == replace) {
                map[i - 1][j - 1] = replacement;
                elake.push(i - 1);
                elake.push(j - 1);
            } else if (map[i - 1][j - 1] == 'X') {
                stackI.push(i - 1);
                map[i - 1][j - 1] = 'I';
                stackI.push(j - 1);
                roundIsland(i-1,j-1);
            }
            if (map[i + 1][j - 1] == replace) {
                map[i + 1][j - 1] = replacement;
                elake.push(i + 1);
                elake.push(j - 1);
            } else if (map[i + 1][j - 1] == 'X') {
                stackI.push(i + 1);
                map[i + 1][j - 1] = 'I';
                stackI.push(j - 1);
                roundIsland(i+1,j-1);
            }
            if (map[i - 1][j + 1] == replace) {
                map[i - 1][j + 1] = replacement;
                elake.push(i - 1);
                elake.push(j + 1);
            } else if (map[i - 1][j + 1] == 'X') {
                stackI.push(i - 1);
                map[i - 1][j + 1] = 'I';
                stackI.push(j + 1);
                roundIsland(i-1,j+1);
            }
            if (map[i + 1][j + 1] == replace) {
                map[i + 1][j + 1] = replacement;
                elake.push(i + 1);
                elake.push(j + 1);
            } else if (map[i + 1][j + 1] == 'X') {
                stackI.push(i + 1);
                map[i + 1][j + 1] = 'I';
                stackI.push(j + 1);
                roundIsland(i+1,j+1);
            }


        }
        if (!stackI.isEmpty()) {
            roundIsland3();
        }
    }

    private static void exploreLake2(char replace, char replacement, int i, int j) {
        map[i][j] = replacement;
        elake.push(i);
        elake.push(j);

        while (elake.size() >= 2) {
            j = elake.pop();
            i = elake.pop();

            if (map[i][j - 1] == replace) {
                map[i][j - 1] = replacement;
                elake.push(i);
                elake.push(j - 1);
            } else if (map[i][j - 1] == 'X') {
                roundIsland2(i, j - 1);
            }
            if (map[i][j + 1] == replace) {
                map[i][j + 1] = replacement;
                elake.push(i);
                elake.push(j + 1);
            } else if (map[i][j + 1] == 'X') {
                roundIsland2(i, j + 1);
            }
            if (map[i - 1][j] == replace) {
                map[i - 1][j] = replacement;
                elake.push(i - 1);
                elake.push(j);
            } else if (map[i - 1][j] == 'X') {
                roundIsland2(i - 1, j);
            }
            if (map[i + 1][j] == replace) {
                map[i + 1][j] = replacement;
                elake.push(i + 1);
                elake.push(j);
            } else if (map[i + 1][j] == 'X') {
                roundIsland2(i + 1, j);
            }
            if (map[i - 1][j - 1] == replace) {
                map[i - 1][j - 1] = replacement;
                elake.push(i - 1);
                elake.push(j - 1);
            } else if (map[i - 1][j - 1] == 'X') {
                roundIsland2(i - 1, j - 1);
            }
            if (map[i + 1][j - 1] == replace) {
                map[i + 1][j - 1] = replacement;
                elake.push(i + 1);
                elake.push(j - 1);
            } else if (map[i + 1][j - 1] == 'X') {
                roundIsland2(i + 1, j - 1);
            }
            if (map[i - 1][j + 1] == replace) {
                map[i - 1][j + 1] = replacement;
                elake.push(i - 1);
                elake.push(j + 1);
            } else if (map[i - 1][j + 1] == 'X') {
                roundIsland2(i - 1, j + 1);
            }
            if (map[i + 1][j + 1] == replace) {
                map[i + 1][j + 1] = replacement;
                elake.push(i + 1);
                elake.push(j + 1);
            } else if (map[i + 1][j + 1] == 'X') {
                roundIsland2(i + 1, j + 1);
            }

        }
    }

    private static void exploreLake(char replace, char replacement, Integer x, Integer y) {
        if (map[x][y] == replace) {
            map[x][y] = replacement;

            exploreLake(replace, replacement, x + 1, y);
            exploreLake(replace, replacement, x - 1, y);
            exploreLake(replace, replacement, x, y - 1);
            exploreLake(replace, replacement, x, y + 1);

            exploreLake(replace, replacement, x + 1, y + 1);
            exploreLake(replace, replacement, x - 1, y + 1);
            exploreLake(replace, replacement, x - 1, y - 1);
            exploreLake(replace, replacement, x + 1, y - 1);
        } else if (map[x][y] == 'X') {
            roundIsland2(x, y);
        }
    }

    private static void replaceFields2(char replace, char replacement, int i, int j) {
        map[i][j] = replacement;
        elake.push(i);
        elake.push(j);

        while (elake.size() >= 2) {
            j = elake.pop();
            i = elake.pop();

            if (map[i][j - 1] == replace) {
                map[i][j - 1] = replacement;
                elake.push(i);
                elake.push(j - 1);
            }
            if (map[i][j + 1] == replace) {
                map[i][j + 1] = replacement;
                elake.push(i);
                elake.push(j + 1);
            }
            if (map[i - 1][j] == replace) {
                map[i - 1][j] = replacement;
                elake.push(i - 1);
                elake.push(j);
            }
            if (map[i + 1][j] == replace) {
                map[i + 1][j] = replacement;
                elake.push(i + 1);
                elake.push(j);
            }
            if (map[i - 1][j - 1] == replace) {
                map[i - 1][j - 1] = replacement;
                elake.push(i - 1);
                elake.push(j - 1);
            }
            if (map[i + 1][j - 1] == replace) {
                map[i + 1][j - 1] = replacement;
                elake.push(i + 1);
                elake.push(j - 1);
            }
            if (map[i - 1][j + 1] == replace) {
                map[i - 1][j + 1] = replacement;
                elake.push(i - 1);
                elake.push(j + 1);
            }
            if (map[i + 1][j + 1] == replace) {
                map[i + 1][j + 1] = replacement;
                elake.push(i + 1);
                elake.push(j + 1);
            }

        }

    }

    private static void exploreOcean6() {


        for (int i = 0; i < h; i++) {
            if (i == 0 || i == h - 1) {
                for (int j = 0; j < w; j++) {
                    map[i][j] = 'o';
                    ocean.push(i);
                    ocean.push(j);
                }
            } else {
                map[i][0] = 'o';
                ocean.push(i);
                ocean.push(0);

                map[i][w - 1] = 'o';
                ocean.push(i);
                ocean.push(w - 1);
            }
        }


        int i, j, it, jt;
        char ch;
        while (ocean.size() >= 2) {
            j = ocean.pop();
            i = ocean.pop();

            it = i;
            jt = j - 1;
            if (isValidCoords(it, jt)) {
                ch = map[it][jt];
                if (ch == '.') {
                    map[it][jt] = 'o';
                    ocean.push(it);
                    ocean.push(jt);
                }
//                else if (ch == 'X') {
//                    island.push(it);
//                    island.push(jt);
//                }
            }
            it = i;
            jt = j + 1;
            if (isValidCoords(it, jt)) {
                ch = map[it][jt];
                if (ch == '.') {
                    map[it][jt] = 'o';
                    ocean.push(it);
                    ocean.push(jt);
                }
//                else if (ch == 'X') {
//                    island.push(it);
//                    island.push(jt);
//                }
            }

            it = i - 1;
            jt = j;
            if (isValidCoords(it, jt)) {
                ch = map[it][jt];
                if (ch == '.') {
                    map[it][jt] = 'o';
                    ocean.push(it);
                    ocean.push(jt);
                }
//                else if (ch == 'X') {
//                    island.push(it);
//                    island.push(jt);
//                }
            }
            it = i + 1;
            jt = j;
            if (isValidCoords(it, jt)) {
                ch = map[it][jt];
                if (ch == '.') {
                    map[it][jt] = 'o';
                    ocean.push(it);
                    ocean.push(jt);
                }
//                else if (ch == 'X') {
//                    island.push(it);
//                    island.push(jt);
//                }
            }

            it = i - 1;
            jt = j - 1;
            if (isValidCoords(it, jt)) {
                ch = map[it][jt];
                if (ch == '.') {
                    map[it][jt] = 'o';
                    ocean.push(it);
                    ocean.push(jt);
                }
//                else if (ch == 'X') {
//                    island.push(it);
//                    island.push(jt);
//                }
            }
            it = i + 1;
            jt = j - 1;
            if (isValidCoords(it, jt)) {
                ch = map[it][jt];
                if (ch == '.') {
                    map[it][jt] = 'o';
                    ocean.push(it);
                    ocean.push(jt);
                }
//                else if (ch == 'X') {
//                    island.push(it);
//                    island.push(jt);
//                }
            }
            it = i - 1;
            jt = j + 1;
            if (isValidCoords(it, jt)) {
                ch = map[it][jt];
                if (ch == '.') {
                    map[it][jt] = 'o';
                    ocean.push(it);
                    ocean.push(jt);
                }
//                else if (ch == 'X') {
//                    island.push(it);
//                    island.push(jt);
//                }
            }
            it = i + 1;
            jt = j + 1;
            if (isValidCoords(it, jt)) {
                ch = map[it][jt];
                if (ch == '.') {
                    map[it][jt] = 'o';
                    ocean.push(it);
                    ocean.push(jt);
                }
//                else if (ch == 'X') {
//                    island.push(it);
//                    island.push(jt);
//                }
            }
        }
    }


    private static void exploreOcean5() {


        for (int i = 0; i < h; i++) {
            if (i == 0 || i == h - 1) {
                for (int j = 0; j < w; j++) {
                    map[i][j] = 'o';
                    ocean.push(i);
                    ocean.push(j);
                }
            } else {
                map[i][0] = 'o';
                ocean.push(i);
                ocean.push(0);

                map[i][w - 1] = 'o';
                ocean.push(i);
                ocean.push(w - 1);
            }
        }
        int i, j;
        while (ocean.size() >= 2) {
            j = ocean.pop();
            i = ocean.pop();

            if (isValidCoords(i, j - 1) && map[i][j - 1] == '.') {
                map[i][j - 1] = 'o';
                ocean.push(i);
                ocean.push(j - 1);
            } else if (isValidCoords(i, j - 1) && map[i][j - 1] == 'X') {
                island.push(i);
                island.push(j - 1);
            }
            if (isValidCoords(i, j + 1) && map[i][j + 1] == '.') {
                map[i][j + 1] = 'o';
                ocean.push(i);
                ocean.push(j + 1);
            } else if (isValidCoords(i, j + 1) && map[i][j + 1] == 'X') {
                island.push(i);
                island.push(j + 1);
            }
            if (isValidCoords(i - 1, j) && map[i - 1][j] == '.') {
                map[i - 1][j] = 'o';
                ocean.push(i - 1);
                ocean.push(j);
            } else if (isValidCoords(i - 1, j) && map[i - 1][j] == 'X') {
                island.push(i - 1);
                island.push(j);
            }
            if (isValidCoords(i + 1, j) && map[i + 1][j] == '.') {
                map[i + 1][j] = 'o';
                ocean.push(i + 1);
                ocean.push(j);
            } else if (isValidCoords(i + 1, j) && map[i + 1][j] == 'X') {
                island.push(i + 1);
                island.push(j);
            }
            if (isValidCoords(i - 1, j - 1) && map[i - 1][j - 1] == '.') {
                map[i - 1][j - 1] = 'o';
                ocean.push(i - 1);
                ocean.push(j - 1);
            } else if (isValidCoords(i - 1, j - 1) && map[i - 1][j - 1] == 'X') {
                island.push(i - 1);
                island.push(j - 1);
            }
            if (isValidCoords(i + 1, j - 1) && map[i + 1][j - 1] == '.') {
                map[i + 1][j - 1] = 'o';
                ocean.push(i + 1);
                ocean.push(j - 1);
            } else if (isValidCoords(i + 1, j - 1) && map[i + 1][j - 1] == 'X') {
                island.push(i + 1);
                island.push(j - 1);
            }
            if (isValidCoords(i - 1, j + 1) && map[i - 1][j + 1] == '.') {
                map[i - 1][j + 1] = 'o';
                ocean.push(i - 1);
                ocean.push(j + 1);
            } else if (isValidCoords(i - 1, j + 1) && map[i - 1][j + 1] == 'X') {
                island.push(i - 1);
                island.push(j + 1);
            }
            if (isValidCoords(i + 1, j + 1) && map[i + 1][j + 1] == '.') {
                map[i + 1][j + 1] = 'o';
                ocean.push(i + 1);
                ocean.push(j + 1);
            } else if (isValidCoords(i + 1, j + 1) && map[i + 1][j + 1] == 'X') {
                island.push(i + 1);
                island.push(j + 1);
            }

        }
    }

    private static void exploreOcean4() {


        for (int i = 0; i < h; i++) {
            if (i == 0 || i == h - 1) {
                for (int j = 0; j < w; j++) {
                    map[i][j] = 'o';
                    ocean.push(i);
                    ocean.push(j);
                }
            } else {
                map[i][0] = 'o';
                ocean.push(i);
                ocean.push(0);

                map[i][w - 1] = 'o';
                ocean.push(i);
                ocean.push(w - 1);
            }
        }
        int i, j;
        while (ocean.size() >= 2) {
            j = ocean.pop();
            i = ocean.pop();

            if (isValidCoords(i, j - 1) && map[i][j - 1] == '.') {
                map[i][j - 1] = 'o';
                ocean.push(i);
                ocean.push(j - 1);
            } else if (isValidCoords(i, j - 1) && map[i][j - 1] == 'X') {
                island.push(i);
                island.push(j - 1);
                map[i][j - 1] = 'C';
            }
            if (isValidCoords(i, j + 1) && map[i][j + 1] == '.') {
                map[i][j + 1] = 'o';
                ocean.push(i);
                ocean.push(j + 1);
            } else if (isValidCoords(i, j + 1) && map[i][j + 1] == 'X') {
                island.push(i);
                map[i][j + 1] = 'C';
                island.push(j + 1);
            }
            if (isValidCoords(i - 1, j) && map[i - 1][j] == '.') {
                map[i - 1][j] = 'o';
                ocean.push(i - 1);
                ocean.push(j);
            } else if (isValidCoords(i - 1, j) && map[i - 1][j] == 'X') {
                island.push(i - 1);
                map[i - 1][j] = 'C';
                island.push(j);
            }
            if (isValidCoords(i + 1, j) && map[i + 1][j] == '.') {
                map[i + 1][j] = 'o';
                ocean.push(i + 1);
                ocean.push(j);
            } else if (isValidCoords(i + 1, j) && map[i + 1][j] == 'X') {
                island.push(i + 1);
                map[i + 1][j] = 'C';
                island.push(j);
            }
            if (isValidCoords(i - 1, j - 1) && map[i - 1][j - 1] == '.') {
                map[i - 1][j - 1] = 'o';
                ocean.push(i - 1);
                ocean.push(j - 1);
            } else if (isValidCoords(i - 1, j - 1) && map[i - 1][j - 1] == 'X') {
                island.push(i - 1);
                map[i - 1][j - 1] = 'C';
                island.push(j - 1);
            }
            if (isValidCoords(i + 1, j - 1) && map[i + 1][j - 1] == '.') {
                map[i + 1][j - 1] = 'o';
                ocean.push(i + 1);
                ocean.push(j - 1);
            } else if (isValidCoords(i + 1, j - 1) && map[i + 1][j - 1] == 'X') {
                island.push(i + 1);
                map[i + 1][j - 1] = 'C';
                island.push(j - 1);
            }
            if (isValidCoords(i - 1, j + 1) && map[i - 1][j + 1] == '.') {
                map[i - 1][j + 1] = 'o';
                ocean.push(i - 1);
                ocean.push(j + 1);
            } else if (isValidCoords(i - 1, j + 1) && map[i - 1][j + 1] == 'X') {
                island.push(i - 1);
                map[i - 1][j + 1] = 'C';
                island.push(j + 1);
            }
            if (isValidCoords(i + 1, j + 1) && map[i + 1][j + 1] == '.') {
                map[i + 1][j + 1] = 'o';
                ocean.push(i + 1);
                ocean.push(j + 1);
            } else if (isValidCoords(i + 1, j + 1) && map[i + 1][j + 1] == 'X') {
                island.push(i + 1);
                map[i + 1][j + 1] = 'C';
                island.push(j + 1);
            }

        }
    }

    private static void exploreOcean3() {


        for (int i = 0; i < h; i++) {
            if (i == 0 || i == h - 1) {
                for (int j = 0; j < w; j++) {
                    map[i][j] = 'o';
                    ocean.push(i);
                    ocean.push(j);
                }
            } else {
                map[i][0] = 'o';
                ocean.push(i);
                ocean.push(0);

                map[i][w - 1] = 'o';
                ocean.push(i);
                ocean.push(w - 1);
            }
        }
        int i, j;
        while (ocean.size() >= 2) {
            j = ocean.pop();
            i = ocean.pop();

            if (isValidCoords(i, j - 1) && map[i][j - 1] == '.') {
                map[i][j - 1] = 'o';
                ocean.push(i);
                ocean.push(j - 1);
            }
            if (isValidCoords(i, j + 1) && map[i][j + 1] == '.') {
                map[i][j + 1] = 'o';
                ocean.push(i);
                ocean.push(j + 1);
            }
            if (isValidCoords(i - 1, j) && map[i - 1][j] == '.') {
                map[i - 1][j] = 'o';
                ocean.push(i - 1);
                ocean.push(j);
            }
            if (isValidCoords(i + 1, j) && map[i + 1][j] == '.') {
                map[i + 1][j] = 'o';
                ocean.push(i + 1);
                ocean.push(j);
            }
            if (isValidCoords(i - 1, j - 1) && map[i - 1][j - 1] == '.') {
                map[i - 1][j - 1] = 'o';
                ocean.push(i - 1);
                ocean.push(j - 1);
            }
            if (isValidCoords(i + 1, j - 1) && map[i + 1][j - 1] == '.') {
                map[i + 1][j - 1] = 'o';
                ocean.push(i + 1);
                ocean.push(j - 1);
            }
            if (isValidCoords(i - 1, j + 1) && map[i - 1][j + 1] == '.') {
                map[i - 1][j + 1] = 'o';
                ocean.push(i - 1);
                ocean.push(j + 1);
            }
            if (isValidCoords(i + 1, j + 1) && map[i + 1][j + 1] == '.') {
                map[i + 1][j + 1] = 'o';
                ocean.push(i + 1);
                ocean.push(j + 1);
            }

        }
    }

    private static void exploreOcean2() {
        for (int i = 1; i < h - 1; i++) {
            for (int j = 1; j < w - 1; j++) {
                if (map[i][j] == 'o') {
                    continue;
                } else if (map[i][j] == 'X') {
                    continue;
                } else if (map[i][j] == '.' && map[i][j - 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i][j + 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i - 1][j] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i + 1][j] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i - 1][j - 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i + 1][j - 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i - 1][j + 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i + 1][j + 1] == 'o') {
                    map[i][j] = 'o';
                }
            }
        }
        for (int i = h - 1 - 1; i >= 1; i--) {
            for (int j = w - 1 - 1; j >= 1; j--) {
                if (map[i][j] == 'o') {
                    continue;
                } else if (map[i][j] == 'X') {
                    continue;
                } else if (map[i][j] == '.' && map[i][j - 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i][j + 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i - 1][j] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i + 1][j] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i - 1][j - 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i + 1][j - 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i - 1][j + 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i + 1][j + 1] == 'o') {
                    map[i][j] = 'o';
                }
            }
        }
        for (int i = 1; i < h - 1 - 1 + 1; i++) {
            for (int j = w - 1 - 1; j >= 1; j--) {
                if (map[i][j] == 'o') {
                    continue;
                } else if (map[i][j] == 'X') {
                    continue;
                } else if (map[i][j] == '.' && map[i][j - 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i][j + 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i - 1][j] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i + 1][j] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i - 1][j - 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i + 1][j - 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i - 1][j + 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i + 1][j + 1] == 'o') {
                    map[i][j] = 'o';
                }
            }
        }
        for (int i = 1; i < h - 1 - 1 + 1; i++) {
            for (int j = w - 1 - 1; j >= 1; j--) {
                if (map[i][j] == 'o') {
                    continue;
                } else if (map[i][j] == 'X') {
                    continue;
                } else if (map[i][j] == '.' && map[i][j - 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i][j + 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i - 1][j] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i + 1][j] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i - 1][j - 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i + 1][j - 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i - 1][j + 1] == 'o') {
                    map[i][j] = 'o';
                } else if (map[i][j] == '.' && map[i + 1][j + 1] == 'o') {
                    map[i][j] = 'o';
                }
            }
        }
    }

    private static void exploreOcean(int x, int y) {

        if (!isValidCoords(x, y) || map[x][y] == 'X') {
            return;
        } else if (map[x][y] == '.') {
            map[x][y] = 'o';

            exploreOcean(x + 1, y);
            exploreOcean(x - 1, y);
            exploreOcean(x, y - 1);
            exploreOcean(x, y + 1);

            exploreOcean(x + 1, y + 1);
            exploreOcean(x - 1, y + 1);
            exploreOcean(x - 1, y - 1);
            exploreOcean(x + 1, y - 1);
        }
    }

//    private static void replaceFields(char replace, char replacement, int x, int y) {
//        int i, j, it, jt;
//        char ch;
//        while (ocean.size() >= 2) {
//            j = ocean.pop();
//            i = ocean.pop();
//
//            it = i;
//            jt = j - 1;
//            if (isValidCoords(it, jt)) {
//                ch = map[it][jt];
//                if (ch == '.') {
//                    map[it][jt] = 'o';
//                    ocean.push(it);
//                    ocean.push(jt);
//                }
////                else if (ch == 'X') {
////                    island.push(it);
////                    island.push(jt);
////                }
//            }
//            it = i;
//            jt = j + 1;
//            if (isValidCoords(it, jt)) {
//                ch = map[it][jt];
//                if (ch == '.') {
//                    map[it][jt] = 'o';
//                    ocean.push(it);
//                    ocean.push(jt);
//                }
////                else if (ch == 'X') {
////                    island.push(it);
////                    island.push(jt);
////                }
//            }
//
//            it = i - 1;
//            jt = j;
//            if (isValidCoords(it, jt)) {
//                ch = map[it][jt];
//                if (ch == '.') {
//                    map[it][jt] = 'o';
//                    ocean.push(it);
//                    ocean.push(jt);
//                }
////                else if (ch == 'X') {
////                    island.push(it);
////                    island.push(jt);
////                }
//            }
//            it = i + 1;
//            jt = j;
//            if (isValidCoords(it, jt)) {
//                ch = map[it][jt];
//                if (ch == '.') {
//                    map[it][jt] = 'o';
//                    ocean.push(it);
//                    ocean.push(jt);
//                }
////                else if (ch == 'X') {
////                    island.push(it);
////                    island.push(jt);
////                }
//            }
//
//            it = i - 1;
//            jt = j - 1;
//            if (isValidCoords(it, jt)) {
//                ch = map[it][jt];
//                if (ch == '.') {
//                    map[it][jt] = 'o';
//                    ocean.push(it);
//                    ocean.push(jt);
//                }
////                else if (ch == 'X') {
////                    island.push(it);
////                    island.push(jt);
////                }
//            }
//            it = i + 1;
//            jt = j - 1;
//            if (isValidCoords(it, jt)) {
//                ch = map[it][jt];
//                if (ch == '.') {
//                    map[it][jt] = 'o';
//                    ocean.push(it);
//                    ocean.push(jt);
//                }
////                else if (ch == 'X') {
////                    island.push(it);
////                    island.push(jt);
////                }
//            }
//            it = i - 1;
//            jt = j + 1;
//            if (isValidCoords(it, jt)) {
//                ch = map[it][jt];
//                if (ch == '.') {
//                    map[it][jt] = 'o';
//                    ocean.push(it);
//                    ocean.push(jt);
//                }
////                else if (ch == 'X') {
////                    island.push(it);
////                    island.push(jt);
////                }
//            }
//            it = i + 1;
//            jt = j + 1;
//            if (isValidCoords(it, jt)) {
//                ch = map[it][jt];
//                if (ch == '.') {
//                    map[it][jt] = 'o';
//                    ocean.push(it);
//                    ocean.push(jt);
//                }
////                else if (ch == 'X') {
////                    island.push(it);
////                    island.push(jt);
////                }
//            }
//        }
//    }

    //    private static void replaceFieldsrec(char replace, char replacement, int x, int y) {
//        if (map[x][y] == replace) {
//            map[x][y] = replacement;
//
//            replaceFields(replace, replacement, x + 1, y);
//            replaceFields(replace, replacement, x - 1, y);
//            replaceFields(replace, replacement, x, y - 1);
//            replaceFields(replace, replacement, x, y + 1);
//
//            replaceFields(replace, replacement, x + 1, y + 1);
//            replaceFields(replace, replacement, x - 1, y + 1);
//            replaceFields(replace, replacement, x - 1, y - 1);
//            replaceFields(replace, replacement, x + 1, y - 1);
//        }
//
//    }
    private static void roundIsland3() {
        int j, i;
        while (stackI.size() >= 2) {
            j = stackI.pop();
            i = stackI.pop();
            map[i][j] = 'I';

            if (map[i][j - 1] == 'X') {
                map[i][j - 1] = 'I';
                stackI.push(i);
                stackI.push(j - 1);
            } else if (map[i][j - 1] == '.') {
                lakeFoundlist.push(i);
                lakeFoundlist.push(j - 1);
            }
            if (map[i][j + 1] == 'X') {
                map[i][j + 1] = 'I';
                stackI.push(i);
                stackI.push(j + 1);
            } else if (map[i][j + 1] == '.') {
                lakeFoundlist.push(i);
                lakeFoundlist.push(j + 1);
            }
            if (map[i - 1][j] == 'X') {
                map[i - 1][j] = 'I';
                stackI.push(i - 1);
                stackI.push(j);
            } else if (map[i - 1][j] == '.') {
                lakeFoundlist.push(i - 1);
                lakeFoundlist.push(j);
            }
            if (map[i + 1][j] == 'X') {
                map[i + 1][j] = 'I';
                stackI.push(i + 1);
                stackI.push(j);
            } else if (map[i + 1][j] == '.') {
                lakeFoundlist.push(i + 1);
                lakeFoundlist.push(j);
            }
            if (map[i - 1][j - 1] == 'X') {
                map[i - 1][j - 1] = 'I';
                stackI.push(i - 1);
                stackI.push(j - 1);
            } else if (map[i - 1][j - 1] == '.') {
                lakeFoundlist.push(i - 1);
                lakeFoundlist.push(j - 1);
            }
            if (map[i + 1][j - 1] == 'X') {
                map[i + 1][j - 1] = 'I';
                stackI.push(i + 1);
                stackI.push(j - 1);
            } else if (map[i + 1][j - 1] == '.') {
                lakeFoundlist.push(i + 1);
                lakeFoundlist.push(j - 1);
            }
            if (map[i - 1][j + 1] == 'X') {
                map[i - 1][j + 1] = 'I';
                stackI.push(i - 1);
                stackI.push(j + 1);
            } else if (map[i - 1][j + 1] == '.') {
                lakeFoundlist.push(i - 1);
                lakeFoundlist.push(j + 1);
            }
            if (map[i + 1][j + 1] == 'X') {
                map[i + 1][j + 1] = 'I';
                stackI.push(i + 1);
                stackI.push(j + 1);
            } else if (map[i + 1][j + 1] == '.') {
                lakeFoundlist.push(i + 1);
                lakeFoundlist.push(j + 1);
                while (lakeFoundlist.size() >= 2) {
                    j = lakeFoundlist.pop();
                    i = lakeFoundlist.pop();
                    if (map[i][j] == '.') {
                        lakeFound(i, j);
                    }
                }
            }

        }

    }

    private static void roundIsland3old() {
        int j, i;
        while (stackI.size() >= 2) {
            j = stackI.pop();
            i = stackI.pop();
            map[i][j] = 'I';

            if (map[i][j - 1] == 'X') {
                map[i][j - 1] = 'I';
                stackI.push(i);
                stackI.push(j - 1);
            } else if (map[i][j - 1] == '.') {
                lakeFound(i, j - 1);
            }
            if (map[i][j + 1] == 'X') {
                map[i][j + 1] = 'I';
                stackI.push(i);
                stackI.push(j + 1);
            } else if (map[i][j + 1] == '.') {
                lakeFound(i, j + 1);
            }
            if (map[i - 1][j] == 'X') {
                map[i - 1][j] = 'I';
                stackI.push(i - 1);
                stackI.push(j);
            } else if (map[i - 1][j] == '.') {
                lakeFound(i - 1, j);
            }
            if (map[i + 1][j] == 'X') {
                map[i + 1][j] = 'I';
                stackI.push(i + 1);
                stackI.push(j);
            } else if (map[i + 1][j] == '.') {
                lakeFound(i + 1, j);
            }
            if (map[i - 1][j - 1] == 'X') {
                map[i - 1][j - 1] = 'I';
                stackI.push(i - 1);
                stackI.push(j - 1);
            } else if (map[i - 1][j - 1] == '.') {
                lakeFound(i - 1, j - 1);
            }
            if (map[i + 1][j - 1] == 'X') {
                map[i + 1][j - 1] = 'I';
                stackI.push(i + 1);
                stackI.push(j - 1);
            } else if (map[i + 1][j - 1] == '.') {
                lakeFound(i + 1, j - 1);
            }
            if (map[i - 1][j + 1] == 'X') {
                map[i - 1][j + 1] = 'I';
                stackI.push(i - 1);
                stackI.push(j + 1);
            } else if (map[i - 1][j + 1] == '.') {
                lakeFound(i - 1, j + 1);
            }
            if (map[i + 1][j + 1] == 'X') {
                map[i + 1][j + 1] = 'I';
                stackI.push(i + 1);
                stackI.push(j + 1);
            } else if (map[i + 1][j + 1] == '.') {
                lakeFound(i + 1, j + 1);
            }

        }
    }

    private static void roundIsland2_save(int i, int j) {
        if (map[i][j] == 'I') {
            return;
        }
        map[i][j] = 'I';

        stackI.push(i);
        stackI.push(j);

        while (stackI.size() >= 2) {
            j = stackI.pop();
            i = stackI.pop();

            if (map[i][j - 1] == 'X' || map[i][j - 1] == 'C') {
                map[i][j - 1] = 'I';
                stackI.push(i);
                stackI.push(j - 1);
            } else if (map[i][j - 1] == '.') {
                lakeFound(i, j - 1);
            }
            if (map[i][j + 1] == 'X' || map[i][j + 1] == 'C') {
                map[i][j + 1] = 'I';
                stackI.push(i);
                stackI.push(j + 1);
            } else if (map[i][j + 1] == '.') {
                lakeFound(i, j + 1);
            }
            if (map[i - 1][j] == 'X' || map[i - 1][j] == 'C') {
                map[i - 1][j] = 'I';
                stackI.push(i - 1);
                stackI.push(j);
            } else if (map[i - 1][j] == '.') {
                lakeFound(i - 1, j);
            }
            if (map[i + 1][j] == 'X' || map[i + 1][j] == 'C') {
                map[i + 1][j] = 'I';
                stackI.push(i + 1);
                stackI.push(j);
            } else if (map[i + 1][j] == '.') {
                lakeFound(i + 1, j);
            }
            if (map[i - 1][j - 1] == 'X' || map[i - 1][j - 1] == 'C') {
                map[i - 1][j - 1] = 'I';
                stackI.push(i - 1);
                stackI.push(j - 1);
            } else if (map[i - 1][j - 1] == '.') {
                lakeFound(i - 1, j - 1);
            }
            if (map[i + 1][j - 1] == 'X' || map[i + 1][j - 1] == 'C') {
                map[i + 1][j - 1] = 'I';
                stackI.push(i + 1);
                stackI.push(j - 1);
            } else if (map[i + 1][j - 1] == '.') {
                lakeFound(i + 1, j - 1);
            }
            if (map[i - 1][j + 1] == 'X' || map[i - 1][j + 1] == 'C') {
                map[i - 1][j + 1] = 'I';
                stackI.push(i - 1);
                stackI.push(j + 1);
            } else if (map[i - 1][j + 1] == '.') {
                lakeFound(i - 1, j + 1);
            }
            if (map[i + 1][j + 1] == 'X' || map[i + 1][j + 1] == 'C') {
                map[i + 1][j + 1] = 'I';
                stackI.push(i + 1);
                stackI.push(j + 1);
            } else if (map[i + 1][j + 1] == '.') {
                lakeFound(i + 1, j + 1);
            }

        }
    }

    private static void roundIsland2(int i, int j) {
        if (map[i][j] != 'X') {
            return;
        }
        map[i][j] = 'I';

        stackI.push(i);
        stackI.push(j);

        while (stackI.size() >= 2) {
            j = stackI.pop();
            i = stackI.pop();

            if (map[i][j - 1] == 'X') {
                map[i][j - 1] = 'I';
                stackI.push(i);
                stackI.push(j - 1);
            } else if (map[i][j - 1] == '.') {
                lakeFound(i, j - 1);
            }
            if (map[i][j + 1] == 'X') {
                map[i][j + 1] = 'I';
                stackI.push(i);
                stackI.push(j + 1);
            } else if (map[i][j + 1] == '.') {
                lakeFound(i, j + 1);
            }
            if (map[i - 1][j] == 'X') {
                map[i - 1][j] = 'I';
                stackI.push(i - 1);
                stackI.push(j);
            } else if (map[i - 1][j] == '.') {
                lakeFound(i - 1, j);
            }
            if (map[i + 1][j] == 'X') {
                map[i + 1][j] = 'I';
                stackI.push(i + 1);
                stackI.push(j);
            } else if (map[i + 1][j] == '.') {
                lakeFound(i + 1, j);
            }
            if (map[i - 1][j - 1] == 'X') {
                map[i - 1][j - 1] = 'I';
                stackI.push(i - 1);
                stackI.push(j - 1);
            } else if (map[i - 1][j - 1] == '.') {
                lakeFound(i - 1, j - 1);
            }
            if (map[i + 1][j - 1] == 'X') {
                map[i + 1][j - 1] = 'I';
                stackI.push(i + 1);
                stackI.push(j - 1);
            } else if (map[i + 1][j - 1] == '.') {
                lakeFound(i + 1, j - 1);
            }
            if (map[i - 1][j + 1] == 'X') {
                map[i - 1][j + 1] = 'I';
                stackI.push(i - 1);
                stackI.push(j + 1);
            } else if (map[i - 1][j + 1] == '.') {
                lakeFound(i - 1, j + 1);
            }
            if (map[i + 1][j + 1] == 'X') {
                map[i + 1][j + 1] = 'I';
                stackI.push(i + 1);
                stackI.push(j + 1);
            } else if (map[i + 1][j + 1] == '.') {
                lakeFound(i + 1, j + 1);
            }

        }
    }

    private static void lakeFound(int x, int y) {
//        replaceFields('.', 'L', x, y);
        tempSum++;
        lakes.add(x);
        lakes.add(y);
        exploreLake3('.', 'L', x, y);

    }

    private static void roundIsland(int x, int y) {
        if (map[x][y] == 'X') {
            map[x][y] = 'I';

            roundIsland(x + 1, y);
            roundIsland(x - 1, y);
            roundIsland(x, y - 1);
            roundIsland(x, y + 1);

            roundIsland(x + 1, y + 1);
            roundIsland(x - 1, y + 1);
            roundIsland(x - 1, y - 1);
            roundIsland(x + 1, y - 1);
        }
        if (map[x][y] == '.') {
            // replaceFields('.', 'L', x, y);
//            tempSum++;
            lakes.add(x);
            lakes.add(y);
        }
    }

    static boolean isValidCoords(int x, int y) {
        return (x >= 0) && (y >= 0) && (x < h) && (y < w);
    }
}

//javac -Xlint Main.java; cat ../algold/pub04.in | time -f "\t%E real,\t%U user,\t%S sys" java -Xss50m -Xms64m -Xmx1024m Main | cowsay -d | lolcat